+++
date = "2016-09-07T11:09:39+01:00"
draft = false
title = "Article de test"

+++

Bonjour et voici mon premier article avec **Hugo**, pour le moment c'est plutôt sympa à utiliser, à voir sur le long terme haha

Genre... Voici un code:

```
fn main() {
    println!("Hello World!");
}
```
```
int main(int ac, char **av)
{
    printf("%s", av[0]);

    return (0);
}
```
```
<html>
    <head>
        <title>kek</title>
    </head>
    <body>
        <p>Hello World!</p>
    </body>
</html>
```


{{< tweet 666616452582129664 >}}

{{< youtube w7Ft2ymGmfc >}}

{{< gist spf13 7896402 >}}

{{< speakerdeck 4e8126e72d853c0060001f97 >}}
